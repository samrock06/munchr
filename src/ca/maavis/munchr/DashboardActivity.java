package ca.maavis.munchr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import helpers.ConnectionDetector;
import helpers.DisplayAlertDialog;
import helpers.GPSChecker;
import helpers.Place;
import helpers.PlacesFound;
import helpers.PlacesList;

import java.util.HashMap;
import java.util.List;

import android.widget.ListAdapter;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.Menu;
import android.widget.EditText;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;

import library.DatabaseHandler;
import library.UserFunctions;


public class DashboardActivity extends Activity implements OnQueryTextListener {
	private SearchView searchView;
    
	private TextView statusView;
	ListView listView;
	ConnectionDetector cDetector; // instantiate connection detector from connection detector class
	GPSChecker GPS;// instantiate gps from GPSChecker
	Boolean internetAvailable = false;	// flag for internet status 
	DisplayAlertDialog alertDialog = new DisplayAlertDialog();	// instantiating alertDialog from alert dialog class
	PlacesFound googlePlaces; // instantiate places from places found
	PlacesList listOfNearbyPlaces;
	ArrayList<HashMap<String, String>> listPlaces = new ArrayList<HashMap<String,String>>();// list of places found
	ProgressDialog pDialog;
	EditText search;
	public static String REFERENCE ="reference"; //place id
	public static String NAME = "name";// place name 
	public static String VICINITY ="vicinity";//place area
	public static String ICON =" icon";
	UserFunctions userFunctions;
    DatabaseHandler db;
    
	public void onCreate(Bundle savedInstaceState){
		userFunctions = new UserFunctions();
        db = new DatabaseHandler(getApplicationContext());
		
		//if(userFunctions.isUserLoggedIn(getApplicationContext())){
			super.onCreate(savedInstaceState);
			setContentView(R.layout.activity_dashboard);
			GPS = new GPSChecker(this);
			
			listView = (ListView) findViewById(R.id.homeview); 

			new FindRestaurents().execute();
		
			listView.setOnItemClickListener(new  OnItemClickListener() {

				 public void onItemClick(AdapterView<?> parent, View view,
		                    int position, long id) 			{
				// get infos from selected restaurant
				String reference = ((TextView) view.findViewById(R.id.reference)).getText().toString();
					
				//start intent
				Intent intent = new Intent(getApplicationContext(), SinglePlaceActivity.class);	
				
				intent.putExtra(REFERENCE, reference);
				startActivity(intent);
				}			
			});	
	}
	
	// background task : loading  restaurants 
	class FindRestaurents extends AsyncTask<String, String, String>	{

	     protected void onPreExecute() {
	            super.onPreExecute();
				pDialog = new ProgressDialog(DashboardActivity.this);
				pDialog.setMessage(Html.fromHtml("<b>Please Wait</b><br/>Loading nearby restaurants..."));
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
	     }
		protected String doInBackground(String... args) {
		
		googlePlaces = new PlacesFound();// declaring places
		try{
			String types="restaurant";// list restaurents only
			double radius= 3000;// find places within 3 kms of user location
			double latitude = 44.64283;
			double longitude = -63.5723267;
			//listOfNearbyPlaces = googlePlaces.search(GPS.getLatitude(), GPS.getLongitude(), radius, types);// find nearby restaurents
			listOfNearbyPlaces = googlePlaces.search(latitude, longitude, radius, types);// find nearby restaurents
		}
			catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
		
	protected void onPostExecute(String file_url)	{
		
		pDialog.dismiss();
		runOnUiThread(new Runnable() {// update user interface from background process
			public void run() {
				// determine json response status
				if(listOfNearbyPlaces.status.equals("OK")) 
				{	Log.d("Location detectd !","longitude:"+ GPS.getLatitude() +  "latitude" + GPS.getLongitude());// display location

					if(listOfNearbyPlaces.results != null)// check if result is not empty
					{
						for( Place place: listOfNearbyPlaces.results)
						{
							HashMap<String, String> map = new HashMap<String, String>();
							map.put(REFERENCE, place.reference);//Place full details
							map.put(ICON, place.icon);
							map.put(NAME, place.name);//place name
							listPlaces.add(map);	
						}
						ListAdapter adapter = new SimpleAdapter(DashboardActivity.this, listPlaces, R.layout.list_row, new String[] { REFERENCE, NAME}, new int[]{R.id.reference,  R.id.name});
						listView.setAdapter(adapter);// add places to  listview						
					
					}	
				}
				else if(listOfNearbyPlaces.status.equals("ZERO_RESULTS")){
                    // Zero results found
                    alertDialog.showAlertDialog(DashboardActivity.this, "Nearby Restaurents","No restaurents found!",false);
                }
				else if(listOfNearbyPlaces.status.equals("REQUEST_DENIED"))
                {
                	 alertDialog.showAlertDialog(DashboardActivity.this, "Error", " Request denied",false);
                }
                else if(listOfNearbyPlaces.status.equals("OVER_QUERY_LIMIT"))
                {
                	 alertDialog.showAlertDialog(DashboardActivity.this, " Error","Over query limit ",false);
                }
                else if(listOfNearbyPlaces.status.equals("INVALID_REQUEST"))
                {
                	 alertDialog.showAlertDialog(DashboardActivity.this, "Error","Invalid Request",false);
                }
                else if(listOfNearbyPlaces.status.equals("UNKNOWN_ERROR"))
                {
                	 alertDialog.showAlertDialog(DashboardActivity.this, " Error"," unknown error occured.",false);
                }				
                else
                {
                	 alertDialog.showAlertDialog(DashboardActivity.this, " Error", "error occured.",false);
			
			}
			
		}
	});
	}
	}
	//For overflow menu with settings options
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int duration = Toast.LENGTH_SHORT;

        switch(item.getItemId()){
            case R.id.settings:
                Toast.makeText(getBaseContext(), "You selected Settings", duration).show();
                break;
            case R.id.about:
                Toast.makeText(getBaseContext(), "You selected About", duration).show();
                break;
            case R.id.terms:
                Toast.makeText(getBaseContext(), "You selected Terms", duration).show();
                break;
            case R.id.help:
                Toast.makeText(getBaseContext(), "You selected Help", duration).show();
                break;
            case R.id.logout:
            	userFunctions.logoutUser(getApplicationContext());
                Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(login);
                // Closing dashboard screen
                finish();
                break;
        }

        return true;
    }
    private void setupSearchView(MenuItem searchItem) {
		if(isAlwaysExpanded()){
            searchView.setIconifiedByDefault(false);
        }
        else{
            searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(searchManager != null){
            List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();
 
            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
            for (SearchableInfo inf : searchables) {
                if (inf.getSuggestAuthority() != null
                        && inf.getSuggestAuthority().startsWith("applications")) {
                    info = inf;
                }
            }
            searchView.setSearchableInfo(info);
        }
        searchView.setOnQueryTextListener(this);
	}

    public boolean onQueryTextChange(String newText) {
        statusView.setText("Query = " + newText);
        return false;
    }
 
    public boolean onQueryTextSubmit(String query) {
        statusView.setText("Query = " + query + " : submitted");
        return false;
    }
 
    public boolean onClose() {
        statusView.setText("Closed!");
        return false;
    }
 
    protected boolean isAlwaysExpanded() {
        return false;
    }
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
    	
        getMenuInflater().inflate(R.menu.activity_login, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        setupSearchView(searchItem);
        return true;
	}
}