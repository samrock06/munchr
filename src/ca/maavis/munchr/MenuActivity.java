package ca.maavis.munchr;

import java.util.ArrayList;
import java.util.HashMap;

import library.DatabaseHandler;
import library.UserFunctions;

import android.app.Activity;
//import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MenuActivity extends Activity {
	
	UserFunctions userFunctions;
    DatabaseHandler db;
    ListView v;
    
    public static String CATEGORY="Category"; //place id
	
	public static String ITEM= "Item";// place name 
	
	public static String PRICE="Price";//place area
	public static String ID = "id";//place area
	
	public static ArrayList<HashMap<String, String>> mapitems;
    
	public void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.restaurant_menu);
		
		userFunctions = new UserFunctions();
		db = new DatabaseHandler(getApplicationContext());
		
		//Intent intent = getIntent();
		
		v = (ListView) findViewById(R.id.restaurant_items);
   	 	
   	 	mapitems = db.getMenuDetails();
   	 	
   	 	Log.e("Size", mapitems.toString());
   	 	
   	 	ListAdapter adapter = new SimpleAdapter(MenuActivity.this, mapitems, R.layout.menu_items_list, new String[] {ITEM, PRICE}, new int[]{R.id.item_name,  R.id.item_price});
   	 	
   	 	v.setAdapter(adapter);
   	 	
	}
	public void onCheckBoxClicked(View v){
		int duration = Toast.LENGTH_SHORT;
		Toast.makeText(getBaseContext(), "You selected Terms", duration).show();
	}

}
