package helpers;
 
import org.apache.http.client.HttpResponseException;
 
import android.util.Log;
 
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.googleapis.GoogleHeaders;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.http.json.JsonHttpParser;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
 
@SuppressWarnings("deprecation")

public class PlacesFound {
	
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	
	//google API key
	private static final String API_KEY="AIzaSyDjgT6sTYndjJ8F1urgmpuRPgMlG5WKSps";
	
	// search url's 
	 private static final String PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
	   // private static final String PLACES_TEXT_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
	    private static final String PLACES_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json?";
	 
	    private double _latitude; // latitude of place
	    private double _longitude;// longitude of place
	    private double _radius; // radius of searchable area

	    
	    public PlacesFound(){
	    	
	    }
	    
	    // search for places 
	    public PlacesList search (double latitude, double longitude, double radius, String type) throws Exception {
	    	this._latitude = latitude;
	    	this._longitude = longitude;
	    	this._radius = radius;
	    	try{
	    			HttpRequestFactory httpRequestFactory = createRequestFactory(HTTP_TRANSPORT);
	    			HttpRequest request = httpRequestFactory.buildGetRequest(new GenericUrl(PLACES_SEARCH_URL));
	    			
	    			request.getUrl().put("key", API_KEY);
	    			request.getUrl().put("location", _latitude + "," + _longitude);
	    			request.getUrl().put("radius", _radius);
	    			request.getUrl().put("sensor", "false");
	    			if(type != null){
	    				request.getUrl().put("types", type);
	    			}
	    			
	    			PlacesList list = request.execute().parseAs(PlacesList.class);
	    			
	    			Log.d("Restaurants Status",""+list.status); // check log cat for places response status
	    			return list;
	    			
	    	}catch (HttpResponseException e){
	    		Log.e("error",e.getMessage());
	    		return null;
	    	}
	    }
	    
	    //get informations about single restaurent
	    
	    public PlaceInfo getPlaceDetails(String reference) throws Exception {
	        try {
	 
	            HttpRequestFactory httpRequestFactory = createRequestFactory(HTTP_TRANSPORT);
	            HttpRequest request = httpRequestFactory
	                    .buildGetRequest(new GenericUrl(PLACES_DETAILS_URL));
	            request.getUrl().put("key", API_KEY);
	            request.getUrl().put("reference", reference);
	            request.getUrl().put("sensor", "false");
	 
	            PlaceInfo place = request.execute().parseAs(PlaceInfo.class);
	 
	            return place;
	 
	        } catch (HttpResponseException e) {
	            Log.e("Error in Perform Details", e.getMessage());
	            throw e;
	        }
	    }

	   
	    
	    //create http request factory
	    public static HttpRequestFactory createRequestFactory(
	            final HttpTransport transport) {
	        return transport.createRequestFactory(new HttpRequestInitializer() {
	            public void initialize(HttpRequest request) {
	                GoogleHeaders headers = new GoogleHeaders();
	                headers.setApplicationName("MunchMonstar");
	                request.setHeaders(headers);
					JsonHttpParser parser = new JsonHttpParser(new JacksonFactory());
	                request.addParser(parser);
	            }
	        });
	    }
	 
	}	    		
	    			
	    			



	    			