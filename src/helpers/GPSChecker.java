package helpers;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

@SuppressLint("Instantiatable")
public class GPSChecker extends Service implements LocationListener {

	private final Context context;
	
	
	boolean isGPSOn =false; // flag for gps status
	boolean isNetOn=false;// flag for network status
	boolean isLocationDetected=false;// flag for network status
	
	Location location; // location
	
	double longitude; // longitude
	double latitude;// latitude
	
	private static final long MinDistChangeUpdates= 10; // update when dist change( 10 meters changed min)
	private static final long MinTimeBwUpdates= 1000* 60 * 1; // update every 2 mins (min)
	
	
	protected LocationManager locationManager; // declare location manager
	
	public GPSChecker (Context cont ) {
		this.context = cont;
		getLocation();
		
	}
	
	
	public Location getLocation() {
	
		try{
			locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
			
			isGPSOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);// get the gps status
			isNetOn= locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);// ge the network status
			
			
			if(!isGPSOn && !isNetOn) // if  network provider disabled 
			{
				
				
			}
			
			else{
				this.isLocationDetected=true;
				if(isNetOn) {
				
						locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MinTimeBwUpdates, MinDistChangeUpdates,this);
						// get  location from the netwrk provider
			
						Log.d("Network", "Network is enabled");
						if(locationManager !=null){
			
								location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);  // store the last knwonw location in location
								
								if(location !=null)
								{
									latitude = location.getLatitude(); // get location latitude and store it in the varible latitude
									longitude= location.getLongitude();// get longitude of the location and store it in the variable longitude

								}
						}
				}
				
				if(isGPSOn){
					if(location == null){
						locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MinTimeBwUpdates, MinDistChangeUpdates,this);
					// get location from gps provider
					
					Log.d("GPS", "GPS is enabled");
					if(locationManager !=null)
					{
						location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						// store the lastknwonlocation in location
						
							if (location!=null){
								latitude = location.getLatitude(); // get location latitude
								longitude = location.getLongitude();//gets the locaiton longitude	
							}
					}
				}
			}
		}
		
		
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return location;
	}
	
	
	
	public double getLatitude()// function to ge the latitude
	{
		
		if(location !=null)
			latitude = location.getLatitude();
			
		return latitude;
	
	}

	public double getLongitude() // function to get the longitude
	{
		if(location!=null)
			{longitude= location.getLongitude();}
		
		return longitude;
	}
	
	public boolean isLocationDetected (){
		return this.isLocationDetected; // method checks if location detected
	}
	
	
	public void displayAlertDialog () {
		
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		
		alertDialog.setTitle("GPS Error ");
		alertDialog.setMessage("the GPS is disabled, to turn it on clik on setting");
		
		// when the user clicks on the settings button , he will be redirected to settings under location settings
		alertDialog.setPositiveButton("Settings",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			context.startActivity(intent);
			}
		});
		
		
		
		// when the user clicks on the later , he will close the dialog without going to settings
		alertDialog.setNegativeButton("Later", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
	
		alertDialog.show(); // show the message
	}
	
	
	//stop using gps
	public void  terminateGPSUsage(){
		if(locationManager!=null){
			locationManager.removeUpdates(GPSChecker.this);

		}
		
	}
	public void onLocationChanged(Location location) {
		
	}

	public void onProviderDisabled(String provider) {
		
	}

	public void onProviderEnabled(String provider) {
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


	
}

