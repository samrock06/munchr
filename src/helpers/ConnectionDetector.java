package helpers;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {

	private Context _context; 
	
	
	// allows access to application specific resources ( in this internet connection)
	public ConnectionDetector (Context context)
	{
		this._context=context;
		
	}

	public boolean isConnectedToInternet()
	{
		
	ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
	// the previous statement returns (networkinfo) instance represtnt the first connected network interface it can find or null
	if(connectivity !=null)
	{	
	NetworkInfo[] networkInfo = connectivity.getAllNetworkInfo();// store the values of networkinfo into active info
	
	if(networkInfo !=null)// if the phone is connected  do this
		for(int i=0; i< networkInfo.length;i++)
			if(networkInfo[i].getState()== NetworkInfo.State.CONNECTED)
				{
					return true;
				}
	}
	return false;
	}
}
